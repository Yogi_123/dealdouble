namespace DealDouble.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class auctionEntityAdded : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Auctions", "StartingTime", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Auctions", "EndTime", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Auctions", "EndTime", c => c.String());
            AlterColumn("dbo.Auctions", "StartingTime", c => c.String());
        }
    }
}
